import java.util.Scanner;

public class PartThree {
  public static void main (String args[]){
    
    //create scanner
    Scanner keyboard = new Scanner(System.in);
    
    //ask the user for the length of the square    
    System.out.println("Please enter the length of the square's side:");
    
    double sqLength = keyboard.nextDouble();
    
    //ask the user for the length of the rectangle    
    System.out.println("Please enter the length of the rectange:");
    
    double reLength = keyboard.nextDouble();
    
    //ask the user for the width of the rectangle
    System.out.println("Please enter the width of the rectange:");
    
    double reWidth = keyboard.nextDouble();
    
    //calculate and print the area of the square
    System.out.println(AreaComputations.areaSquare(sqLength));
    
    //calculate and print the area of the rectangle
    AreaComputations ac = new AreaComputations();
    
    System.out.println(ac.areaRectangle(reLength, reWidth));
  }
}